FROM nginxinc/nginx-unprivileged:1-alpine
LABEL mainteiner="anami127.0.0.1@gmail.com"

# copy local files into our docker container
# nginx expects to find configuration files inside /etc/nginx
COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

# set the environment variables to be populated in the nginx template default.conf.tpl
ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

USER root
RUN mkdir -p /vol/static
RUN chmod 755 /vol/static
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

COPY ./entrypoint.sh /entrypoint.sh
# make entrypoint file executable with +x flag
RUN chmod +x /entrypoint.sh

USER nginx

CMD ["/entrypoint.sh"]


