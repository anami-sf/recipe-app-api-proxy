# Recipe App Api Proxy

## Usage

### Files
`default.conf.tpl` nginx configuration file template. Gets passed to `envsubst` which populates values in the template vased on environment variables stored somewhere else like github for example. 

All static files in `django` are prefixed with `/static`

Directory in the proxy server to serve static files from `/vol/static/`

? what is an alias in nginx

Map a volume to all the static files that Nginx needs to server

Pass all non-static files to the wsgi server with `wsgi_pass`

`entrypoint.sh` shell script used to run the nginx proxy application inside docker
    * populate the nginx template with environment variables
    * start the nginx service (server)
    * move the `default.conf.tpl` file to the etc file in the docker container
    * once the nginx template is populated witht the values of the environment variables we need to output the filled template to `/etc/nginx/conf.d/default.conf` in the docker container. This is the location where nginx expects to find it's configuration file by default. 
    * Run nginx in the foreground - witht the daemon off - so all the logs of and output of the nginx server get printed to the docker out.
### Environment variables

* `LISTEN_PORT` - port to listen on (default: `8000`)
* `APP_HOST` - hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - port of the app to forward requests to (default: `9000`)
